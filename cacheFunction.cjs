function cacheFunction(callback){
    const cache = {};
    let flag = false;
    function invoke(...args){
        for (arguments in args){
            if (args[arguments] in cache){
                console.log(Object.keys(cache))
            }
            else if(flag===false){
                cache[args[arguments]]="used"
                callback(args[arguments]);
                flag = true;
            }
            else{
                cache[args[arguments]]="used"
            }
        }
        flag = false;
    }
    return invoke;
}
function callback(value){
    console.log("I was called because cache didn't have your argument\nI wont be displayed again", value)
}
module.exports = {cacheFunction, callback}
