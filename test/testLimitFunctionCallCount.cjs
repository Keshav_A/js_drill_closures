const {limitFunctionCallCount, callback} = require('../limitFunctionCallCount.cjs')
const closureFunction = limitFunctionCallCount(callback, 5)
closureFunction() // Called for 1st time
closureFunction() // Called for 2nd time
closureFunction() // Called for 3rd time
closureFunction() // Called for 4th time
closureFunction() // Called for 5th time
closureFunction() // Called for 6th time
closureFunction() // Called for 7th time