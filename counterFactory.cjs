module.exports = function counterFactory() {
    let a = 100;
    function increment() {
        a++;
        return a;
    }
    function decrement() {
        a--;
        return a;
    }
    console.log(a);
    return { increment, decrement };
}