function limitFunctionCallCount(callback, times) {
    let counter = 0
    function invoke() {
        if (counter < times) {
            counter++;
            callback(counter)
        }
    }
    return invoke;
}
function callback(value) {
    console.log("called for %d times", value);
}
module.exports = { limitFunctionCallCount, callback };